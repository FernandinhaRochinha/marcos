﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projeto_SIGMA.Telas
{
    public partial class frmInicial : Form
    {
        public frmInicial()
        {
            InitializeComponent();
        }

        private void frmInicial_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmFornecedor tela = new frmFornecedor();
            tela.Show();
           Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmCadastros tela = new frmCadastros();
            tela.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmVeiculoseModelos tela = new frmVeiculoseModelos();
            tela.Show();
            Hide();

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }
    }
}
